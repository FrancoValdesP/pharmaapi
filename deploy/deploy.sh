#!/bin/bash

set -e

eval $(ssh-agent -s)

echo "$PEM" | tr -d '\r' | ssh-add - > /dev/null

# Alternative approach
# echo -e "$PEM" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.
./deploy/hostKeyChecking.sh

# Exec script in $SERVER_IP
echo "deploying to ${SERVER_EC2}"

ssh $AWS_USER@$SERVER "cat > .env << EOF
    COMMUNE_URL=$COMMUNE_URL
    COMMUNE_METHOD=$COMMUNE_METHOD
    COMMUNE_PARAM=$COMMUNE_PARAM
    PHA_URL=$PHA_URL
    PHA_PARA=$PHA_PARA
    PHA_METHOD=$PHA_METHOD
    REGIONS_URL=$REGIONS_URL
    REGIONS_METHOD=$REGIONS_METHOD
    SECRET=$SECRET
EOF
bash" < ./deploy/updateRemote.sh

