#!/bin/bash

set -e

APIPHARMA_TOKEN=$(cat ./key/pharmaToken.txt)

# pm2 delete APIPHARMA
if [[ $(pm2 list | grep APIPHARMA) == *"APIPHARMA"* ]]; then
  pm2 delete APIPHARMA
fi

# Remove folder
rm -rf pharmaApi

# Load token and clone
git clone "https://PHARMA:${APIPHARMA_TOKEN}@gitlab.com/FrancoValdesP/pharmaapi.git"

#mov .env
cp .env pharmaapi/.env

# Go to pharmaapi
cd pharmaapi

# Install packages
npm install

# run prod project
npm run start:prod
