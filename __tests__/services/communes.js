import { getCommunes } from '../../src/services/'

describe('COMMUNES SERVICE', () => {
	it('get communes information from API GOB', async () => {
		const res = await getCommunes(13)
		expect(res).not.toBeNull()
		expect(res.length).toBeGreaterThan(0)
	})
})
