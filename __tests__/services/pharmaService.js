import { extractDataFromApi } from '../../src/services'
import { env } from 'process'

describe('PHARMA SERVICE', () => {
	it('get information from PHARMA service', async () => {
		const url = env.PHA_URL + '?' + env.PHA_PARA + '=2'
		const res = await extractDataFromApi(url, env.PHA_METHOD)
		expect(res).not.toBeNull()
		expect(res.length).toBeGreaterThan(0)
	})
})
