import { extractDataFromApi } from '../../src/services'
import { env } from 'process'

describe('API SERVICE', () => {
	it('get information from any API', async () => {
		const url = env.PHA_URL + '?' + env.PHA_PARA + '=1'
		const res = await extractDataFromApi(url, env.PHA_METHOD)
		expect(res).not.toBeNull()
		expect(res.length).toBeGreaterThan(0)
	})
	it('get null from API fake', async () => {
		const url = 'http://fakeurlpharma.com'
		const res = await extractDataFromApi(url, 'get')
		expect(res).toBeNull()
	})
})
