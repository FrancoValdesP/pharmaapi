import { getRegions } from '../../src/services/'

describe('REGIONS SERVICE', () => {
	it('get regions information from API GOB', async () => {
		const res = await getRegions()
		expect(res).not.toBeNull()
		expect(res.length).toBeGreaterThan(0)
	})
})
