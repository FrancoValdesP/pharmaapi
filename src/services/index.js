import { extractDataFromApi } from './apiService'
import { searchPharmaByIdReg } from './pharmaService'
import { getRegions } from './regionService'
import { getCommunes } from './communeService'

export  {
	extractDataFromApi,
	searchPharmaByIdReg,
	getRegions,
	getCommunes
}