import axios from 'axios'

const extractDataFromApi = async (url, method, options = '') => {
	try {
		return await axios[method](url,
			options
		).then(d => d.data)
	} catch (error) {
		return null
	}
}

export {
	extractDataFromApi
}