import { extractDataFromApi } from './apiService'
import { env } from 'process'
import qs from 'qs'

const getCommunes = async (id) => {
	try {
		if (!id && !Number(id)) return null
		const { COMMUNE_URL, COMMUNE_PARAM, COMMUNE_METHOD } = env
		const options = qs.stringify({ [COMMUNE_PARAM]: id })
		return await extractDataFromApi(COMMUNE_URL, COMMUNE_METHOD, options)
	} catch (err) {
		return null
	}
}

export {
	getCommunes
}