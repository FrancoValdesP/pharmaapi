import { extractDataFromApi } from './apiService'
import { env } from 'process'

const searchPharmaByIdReg = async (id) => {
	try {
		if (!id && !Number(id)) return null
		const url = env.PHA_URL + '?' + env.PHA_PARA + '=' + id
		const pharmaData = await extractDataFromApi(url, env.PHA_METHOD)
		if (!pharmaData.length) return null
		return pharmaData
	} catch (err) {
		return null
	}
}

export {
	searchPharmaByIdReg
}