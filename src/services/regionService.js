import { extractDataFromApi } from './apiService'
import { env } from 'process'

const getRegions = async () => {
	try {
		return await extractDataFromApi(env.REGIONS_URL, env.REGIONS_METHOD)
			.then(data => {
				return data.map(d => {
					d.codigo = d.codigo * 1
					return d
				})
			})
	} catch (err) {
		return err
	}
}

export {
	getRegions
}