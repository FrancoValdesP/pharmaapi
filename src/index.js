import express from 'express'
import cookieParser from 'cookie-parser'
import logger from 'morgan'
import { createServer, } from 'http'
import { env } from 'process' //extraen las principales variables desde el .env
const debug = require('debug')('api:server')
import { Router } from './Router'

const app = express()

//middlers
app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*')
	res.header('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Authorization')
	res.header('Access-Control-Allow-Methods', 'GET,PUT,PATCH,POST,DELETE,OPTIONS')
	next()
})
Router.map((route) => app.use(route.path, route.middleware, route.handler))

const port = normalizePort(env.PORT || '3002')
app.set('port', port)

//se crea el server
const server = createServer(app)

server.listen(port)
server.on('error', onError)
server.on('listening', onListening)

function normalizePort(val) {
	const port = parseInt(val, 10)

	if (isNaN(port)) {
		return val
	}

	if (port >= 0) {
		return port
	}
	return false
}

//mensajes en caso de error
function onError(error) {
	if (error.syscall !== 'listen') {
		throw error
	}

	const bind = typeof port === 'string'
		? 'Pipe ' + port
		: 'Port ' + port

	switch (error.code) {
	case 'EACCES':
		console.error(bind + ' requires elevated privileges')
		process.exit(1)
		break
	case 'EADDRINUSE':
		console.error(bind + ' is already in use')
		process.exit(1)
		break
	default:
		throw error
	}
}

// listener del server
function onListening() {
	const addr = server.address()
	const bind = typeof addr === 'string'
		? 'pipe ' + addr
		: 'port ' + addr.port
	debug('Listening on ' + bind)
}
