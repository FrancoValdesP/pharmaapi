import { Router } from 'express'
import { searchNamesOfPharm, searchPharms  } from '../controllers'

const routes = Router()

routes.get('/locals/', searchPharms)
routes.get('/names/', searchNamesOfPharm)

export default routes
