import pharmaRoutes from './pharma'
import regionsRoutes from './regions'
import communeRoutes from './communes'

export {
	pharmaRoutes,
	regionsRoutes,
	communeRoutes
}