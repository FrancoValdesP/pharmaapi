import { Router } from 'express'
import { getCommunesInformation } from '../controllers'

const routes = Router()

routes.get('/names/', getCommunesInformation)


export default routes
