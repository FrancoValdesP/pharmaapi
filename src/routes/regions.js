import { Router } from 'express'
import { getRegionsInformation } from '../controllers'

const routes = Router()

routes.get('/names/', getRegionsInformation)

export default routes
