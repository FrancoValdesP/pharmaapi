import { pharmaRoutes, regionsRoutes, communeRoutes } from './routes/'

export const Router = [{
	path: '/pharma',
	middleware: [],
	handler: pharmaRoutes
},
{
	path: '/communes',
	middleware: [],
	handler: communeRoutes
},
{
	path: '/regions',
	middleware: [],
	handler: regionsRoutes
}]