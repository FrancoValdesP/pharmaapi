import { searchPharmaByIdReg } from '../services/'

//devuelve la información de las farmacias por nombre y comuna correspondiente
const searchPharms = async (req, res) => {
	const { localName, idCommune, idReg } = req.query
	if (!localName && !idCommune && !idReg)
		return res
			.status(400)
			.send({ message: 'missing required fields ( localName, commune, idReg ).' })
			.end()

	const pharmaData = await searchPharmaByIdReg(idReg)
	if (!pharmaData.length)
		return res
			.status(400)
			.send({ message: 'Without pharmacies' })
			.end()

	let pharmacies = pharmaData
		.map(pharm => {
			if (pharm.fk_comuna * 1 == idCommune && pharm.local_nombre == localName) {
				pharm = {
					localName: pharm.local_nombre,
					direction: pharm.local_direccion,
					telephone: pharm.local_telefono,
					lat: pharm.local_lat,
					lon: pharm.local_lng,
				}
				return pharm
			}
		}, [])
		.filter(p => p != null)
	if (!pharmacies.length) pharmacies = { message: 'Without pharmacies by name: "' + localName + '" and idCommune: ' + idCommune }
	return res
		.send(pharmacies)
		.end()
}

//devuelve los nombres de las farmacias por comuna y region, aplicable a un option-select
const searchNamesOfPharm = async (req, res) => {
	const { idCommune, idReg } = req.query
	if (!idCommune)
		return res
			.status(400)
			.send({ message: 'missing required fields ( idCommune, idReg ).' })
			.end()
	const pharmaData = await searchPharmaByIdReg(idReg)
	if (!pharmaData.length)
		return res
			.status(400)
			.send({ message: 'Without pharmacies in region' })
			.end()

	let namesPharm = pharmaData
		.reduce((acc, curr) => {
			if (curr.fk_comuna * 1 == idCommune) {
				acc.push(curr.local_nombre)
			}
			return acc
		}, [])
	namesPharm = [...new Set(namesPharm)]
	if (!namesPharm.length) namesPharm = { message: 'Without pharmacies in commune' }
	return res
		.send(namesPharm)
		.end()
}

export {
	searchPharms,
	searchNamesOfPharm
}