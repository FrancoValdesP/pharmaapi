import { searchNamesOfPharm, searchPharms } from './pharma'
import { getRegionsInformation } from './region'
import { getCommunesInformation } from './communes'

export {
	searchNamesOfPharm,
	searchPharms,
	getRegionsInformation,
	getCommunesInformation
}