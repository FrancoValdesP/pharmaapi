import { getCommunes } from '../services/'

const getCommunesInformation = async (req, res) => {
	const { idReg } = req.query
	if (!idReg)
		return res
			.status(400)
			.send({ message: 'missing required fields ( idReg ).' })
			.end()
	const communes = await getCommunes(idReg)
	res.send(communes).end()
}

export {
	getCommunesInformation
}