import { getRegions } from '../services/'

const getRegionsInformation = async (req, res) => {
	const regiones = await getRegions()
	res.send(regiones).end()
}

export {
	getRegionsInformation
}