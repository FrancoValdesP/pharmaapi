# API PHARMA
Node.js + ExpressJS + PM2 + Eslint + ES6 + CI/CD + JEST + BABEL + NODEMON + AXIOS + GITFLOW + AWS
------------
# ¿ Que se usa para la aplicación ?
- **NODE** Version 10.16.0.
- **PM2** Para instancia en producción.
- **CI/CD**  A través de GitLab CI.
- **AWS EC2**  Para ambiente en producción.
- **GITFLOW**  A estrucutrar features. 
- **JEST** Para pruebas.
- **Nodemon** para desarrollo.
- **ES6** para promesas async/await.
- **AXIOS** para consumir api.

## Structure
```bash
    __test__ (Pruebas)
    deploy (Archivos para despliegue desde gitlab)
    /src
        /controllers 
        /routes
        /services
        index.js (Archivo de aplicación)
    .env (Archivo de variables de entorno)
    .eslintrc.js (Archivo de configuración lint)
    .gitgnore (Archivo git)
    gitlab-ci.yml (Archivo de configurtacón CI/CD)
    package.json (Archivo de configuraciòn principal del APP)
```

# CI/CD
1. Realizar commit para CI en develop.
2. Realizar commit merge a master para ci/cd.
3. Ver pipelines: https://gitlab.com/FrancoValdesP/pharmaapi/pipelines
4. API en producción: 54.189.162.97:3004

# END POINTS PRO/DEV

https://documenter.getpostman.com/view/4115173/SW17SFhZ

# INSTALL
1. Primero clonar repositorio.
		
		git@gitlab.com:FrancoValdesP/pharmaapi.git
		
2. Descargar dependencias.

		npm install
		
3. Crear archivo .env con la siguente configuración.

```bash
    # PERSONALCONFIGURATION
    SECRET=FR4NC0
    PORT=3004

    #APIS
    COMMUNE_URL=https://midastest.minsal.cl/farmacias/maps/index.php/utilidades/maps_obtener_comunas_por_regiones
    COMMUNE_METHOD=post
    COMMUNE_PARAM=reg_id
    PHA_URL=https://farmanet.minsal.cl/maps/index.php/ws/getLocalesRegion
    PHA_PARA=id_region
    PHA_METHOD=get
    REGIONS_URL=http://apis.digital.gob.cl/dpa/regiones
    REGIONS_METHOD=get
```
# INICIAR APLICACIÓN.
    Se debe tener instalado PM2 global.
### Development: Inicia en modo desarrollo a través de Nodemon.
	npm run dev
### Test: Inicia las pruebas.
	npm test
### Production: Inicia en modo producción, transpilando el proyecto.
	npm run start:prod
